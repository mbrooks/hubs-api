# About
API and tests for Huntsville Urban Bike Share's API

# Project Dependencies

* Node v6 and above
* MySQL

# Install

Install node packages by running the command below

    $ npm Install


This project requires the `hubs` and `hubs_test` database to be created in MySQL. Create them by executing the following:

    $ mysqladmin -u root -p create hubs
    $ mysqladmin -u root -p create hubs_test

Create the hubs mysql user and give it permissions to the database from the mysql CLI:

    mysql> CREATE USER 'hubs'@'%' IDENTIFIED BY 'notforprofit$';
    mysql> grant all on hubs.* to 'hubs'@'%';
    mysql> grant all on hubs_test.* to 'hubs'@'%';

# Usage

The dev server can be started by running the following command:

    $ npm start

Once started, you can connect to the server at the URL: http://localhost:2610/

# Tests

Tests can be run with the following:

    $ npm test
