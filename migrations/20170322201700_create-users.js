
exports.up = function up(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('users', (table) => {
      table.uuid('id').primary();
      table.string('token', 191).unique();
      table.string('firstName');
      table.string('lastName');
      table.string('email', 191).unique();
      table.string('password');
      table.string('driversLicenseNumber');
      table.string('street1');
      table.string('street2');
      table.string('city');
      table.string('state');
      table.string('postalCode');
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
    }),
  ]);
};

exports.down = function down(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('users'),
  ]);
};
