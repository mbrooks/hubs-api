
exports.up = function up(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('checkouts', (table) => {
      table.uuid('id').primary();
      table.string('userId', 191);
      table.string('lockId', 191);
      table.dateTime('checkOutDate').defaultTo(knex.fn.now());
      table.dateTime('checkInDate').defaultTo(null);
      table.dateTime('dueDate');
      table.string('status');
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
    }),
  ]);
};

exports.down = function down(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('checkouts'),
  ]);
};
