
exports.up = function up(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('agreements', (table) => {
      table.uuid('id').primary();
      table.string('userId', 191);
      table.boolean('ofAge');
      table.string('readTermsAndConditions', 2);
      table.string('agreeToWaveRights', 2);
      table.string('version', 191);
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
    }),
  ]);
};

exports.down = function down(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('agreements'),
  ]);
};
