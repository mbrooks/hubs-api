
exports.up = function up(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('locks', (table) => {
      table.uuid('id').primary();
      table.string('name', 191).unique();
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
    }),
  ]);
};

exports.down = function down(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('locks'),
  ]);
};
