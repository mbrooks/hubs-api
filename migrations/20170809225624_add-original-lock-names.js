const uuid = require('uuid');

exports.up = function(knex, Promise) {
  const lockNames = [
    'GOVERNOR',
    'SURFER',
    'DEGREE',
    'SURFACE',
    'MODERN',
    'GEMINI',
    'DAYTIME',
    'RIDE',
    'CABINET',
    'COAT',
    'DIVER',
    'BILL',
    'CONTAIN',
    'PYRAMID',
    'TANK',
    'SCUBA',
  ];
  const queries = lockNames.map((lockName) => {
    return knex('locks').insert({
      id: uuid.v4(),
      name: lockName,
    });
  });

  return Promise.all(queries);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex('locks').del(),
  ]);
};
