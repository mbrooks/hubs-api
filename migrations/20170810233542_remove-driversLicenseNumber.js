
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.alterTable('users', (table) => {
      table.dropColumn('driversLicenseNumber');
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.alterTable('users', (table) => {
      table.string('driversLicenseNumber').after('password');
    }),
  ]);
};
