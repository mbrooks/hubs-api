const uuid = require('uuid');
const db = require('../../lib/db');

const User = db.model('User');

const origUser = {
  token: uuid.v4(),
  firstName: 'bob',
  lastName: 'burkfield',
  email: 'bob@burkfield.bob',
  password: 'bobiscool',
  street1: '123 Bob Street',
  city: 'Bobtown',
  state: 'PA',
  postalCode: '44444',
};

const testUser = {
  user: null,

  getUser() {
    return module.exports.user;
  },

  getBearerToken() {
    return `Bearer ${module.exports.user.token}`;
  },

  setup() {
    return User.create(origUser).then((created) => {
      return User.where('id', created.id).fetch();
    }).then((dahU) => {
      module.exports.user = dahU.toJSON();
    });
  },

  reset() {
    module.exports.user = null;
    return User.destroyAll();
  },
};

module.exports = testUser;
