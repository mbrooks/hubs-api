const assert = require('power-assert');
const supertest = require('supertest-as-promised');
const app = require('../../lib/server');
const db = require('../../lib/db');
const testUserHelper = require('../helpers/testUser');

const User = db.model('User');

describe('the User controller', () => {

  beforeEach(() => {
    return User.destroyAll().then(() => {
      return testUserHelper.setup();
    });
  });

  describe('when POST /user/register is called', () => {
    describe('and no params are sent', () => {

      it('should return 400', () => {
        return supertest(app)
            .post('/user/register')
            .expect('Content-Type', /json/)
            .expect(400)
            .then((res) => {
              assert.equal(res.body.error, 'Bad Request');
            });
      });
    });

    describe('and user already exists', () => {

      let user;

      beforeEach(() => {
        user = testUserHelper.getUser();
      });

      it('should return 400', () => {
        return supertest(app)
            .post('/user/register')
            .send({
              firstName: 'kenny',
              lastName: 'ken',
              email: user.email,
              password: 'kenken123',
              street1: '123 ken street',
              city: 'Kensville',
              state: 'KY',
              postalCode: '33333',
            })
            .expect('Content-Type', /json/)
            .expect(400)
            .then((res) => {
              assert.equal(res.body.message, 'A user has already been registered with the email address specified.');
            });
      });
    });

    describe('and all required params are sent', () => {

      it('should return 201', () => {
        return supertest(app)
            .post('/user/register')
            .send({
              firstName: 'kenny',
              lastName: 'ken',
              email: 'ken@ken.ken',
              password: 'kenken123',
              street1: '123 ken street',
              street2: null,
              city: 'Kensville',
              state: 'KY',
              postalCode: '33333',
            })
            .expect('Content-Type', /json/)
            .expect(201)
            .then((res) => {
              assert.deepEqual(res.body, {
                id: res.body.id,
                token: res.body.token,
                firstName: 'kenny',
                lastName: 'ken',
                email: 'ken@ken.ken',
                street1: '123 ken street',
                street2: null,
                city: 'Kensville',
                state: 'KY',
                postalCode: '33333',
                createdAt: res.body.createdAt,
                updatedAt: res.body.updatedAt,
              });

              // make sure we have a token
              assert(res.body.token);
            });
      });

      describe('and street2 param is set', () => {

        it('should return 201', () => {
          return supertest(app)
              .post('/user/register')
              .send({
                firstName: 'kenny',
                lastName: 'ken',
                email: 'ken@ken.ken',
                password: 'kenken123',
                street1: '123 ken street',
                street2: 'kenpartment 1',
                city: 'Kensville',
                state: 'KY',
                postalCode: '33333',
              })
              .expect('Content-Type', /json/)
              .expect(201)
              .then((res) => {
                assert.deepEqual(res.body, {
                  id: res.body.id,
                  token: res.body.token,
                  firstName: 'kenny',
                  lastName: 'ken',
                  email: 'ken@ken.ken',
                  street1: '123 ken street',
                  street2: 'kenpartment 1',
                  city: 'Kensville',
                  state: 'KY',
                  postalCode: '33333',
                  createdAt: res.body.createdAt,
                  updatedAt: res.body.updatedAt,
                });

                // make sure we have a token
                assert(res.body.token);
              });
        });
      });
    });
  });

  describe('when POST /user/login is called', () => {

    describe('and no params are sent', () => {

      it('should return 400', () => {
        return supertest(app)
            .post('/user/login')
            .expect('Content-Type', /json/)
            .expect(400)
            .then((res) => {
              assert.equal(res.body.error, 'Bad Request');
            });
      });
    });

    describe('and the user exists in db', () => {

      let user;

      beforeEach(() => {
        user = testUserHelper.getUser();
      });

      it('should return the user', () => {
        return supertest(app)
          .post('/user/login')
          .send({
            email: 'bob@burkfield.bob',
            password: 'bobiscool',
          })
          .expect('Content-Type', /json/)
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, {
              id: user.id,
              token: user.token,
              firstName: 'bob',
              lastName: 'burkfield',
              email: 'bob@burkfield.bob',
              street1: '123 Bob Street',
              street2: null,
              city: 'Bobtown',
              state: 'PA',
              postalCode: '44444',
              createdAt: user.createdAt.toISOString(),
              updatedAt: user.updatedAt.toISOString(),
            });
          });
      });
    });

    describe('and the email is invalid', () => {

      beforeEach(() => {
        return User.create({
          firstName: 'kenny',
          lastName: 'ken',
          email: 'ken@ken.ken',
          password: 'kenken123',
          street1: '123 ken street',
          city: 'Kensville',
          state: 'KY',
          postalCode: '33333',
        });
      });

      it('should return the user', () => {
        return supertest(app)
          .post('/user/login')
          .send({
            email: 'fail@me.com',
            password: 'kenken123',
          })
          .expect('Content-Type', /json/)
          .expect(401)
          .then((res) => {
            assert.equal(res.body, 'Unauthorized');
          });
      });
    });
  });

  describe('when GET /user/ping is called', () => {

    describe('and no token provided', () => {

      it('should return 401', () => {
        return supertest(app)
            .get('/user/ping')
            .expect(401);
      });
    });

    describe('and valid token provided', () => {

      let user;

      beforeEach(() => {
        user = testUserHelper.getUser();
      });

      it('should return 200', () => {
        return supertest(app)
            .get('/user/ping')
            .set('Authorization', testUserHelper.getBearerToken())
            .expect('Content-Type', /json/)
            .expect(200)
            .then((res) => {
              assert.deepEqual(res.body, {
                id: user.id,
                token: user.token,
                firstName: 'bob',
                lastName: 'burkfield',
                email: 'bob@burkfield.bob',
                street1: '123 Bob Street',
                street2: null,
                city: 'Bobtown',
                state: 'PA',
                postalCode: '44444',
                createdAt: user.createdAt.toISOString(),
                updatedAt: user.updatedAt.toISOString(),
              });
            });
      });
    });
  });

  describe('when POST /user/:id is called', () => {

    describe('and no token provided', () => {

      it('should return 401', () => {
        return supertest(app)
            .post('/user/fakeid')
            .expect(401);
      });
    });

    describe('and valid token provided with invalid id', () => {

      it('should return 403', () => {
        return supertest(app)
            .post('/user/fakeid')
            .set('Authorization', testUserHelper.getBearerToken())
            .send({
              firstName: 'kenny',
              lastName: 'ken',
              email: 'ken@ken.com',
              street1: '123 ken street',
              city: 'Kensville',
              state: 'KY',
              postalCode: '33333',
            })
            .expect(403);
      });
    });

    describe('and valid token provided with no params', () => {

      let user;

      beforeEach(() => {
        user = testUserHelper.getUser();
      });

      it('should return 400', () => {
        return supertest(app)
            .post(`/user/${user.id}`)
            .set('Authorization', testUserHelper.getBearerToken())
            .expect('Content-Type', /json/)
            .expect(400);
      });
    });

    describe('and valid token provided with valid params', () => {

      let user;

      beforeEach(() => {
        user = testUserHelper.getUser();
      });

      it('should return 200', () => {
        return supertest(app)
            .post(`/user/${user.id}`)
            .set('Authorization', testUserHelper.getBearerToken())
            .expect('Content-Type', /json/)
            .send({
              firstName: 'kenny',
              lastName: 'ken',
              email: 'ken@ken.com',
              street1: '123 ken street',
              city: 'Kensville',
              state: 'KY',
              postalCode: '33333',
            })
            .expect(200)
            .then((res) => {
              assert.deepEqual(res.body, {
                id: user.id,
                token: user.token,
                firstName: 'kenny',
                lastName: 'ken',
                email: 'ken@ken.com',
                street1: '123 ken street',
                city: 'Kensville',
                state: 'KY',
                postalCode: '33333',
                createdAt: user.createdAt.toISOString(),
                updatedAt: user.updatedAt.toISOString(),
              });
            });
      });
    });
  });
});
