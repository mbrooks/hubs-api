const assert = require('power-assert');
const supertest = require('supertest-as-promised');
const tk = require('timekeeper');
const uuid = require('uuid');
const app = require('../../lib/server');
const db = require('../../lib/db');
const testUserHelper = require('../helpers/testUser');

const Checkout = db.model('Checkout');
const Lock = db.model('Lock');
const User = db.model('User');

describe('the Checkout controller', () => {

  beforeEach(() => {
    return Promise.all([
      Checkout.destroyAll(),
      Lock.destroyAll(),
      User.destroyAll(),
    ]).then(() => {
      return testUserHelper.setup();
    });
  });

  afterEach(() => {
    tk.reset();
  });

  describe('when GET /checkouts is called', () => {
    describe('and no token is sent', () => {

      it('should return 401', () => {
        return supertest(app)
          .get('/checkouts')
          .expect(401);
      });
    });

    describe('and valid token is sent', () => {

      let checkout;
      const time = new Date(1490391420000); // 2017-03-24T21:37:00.000Z

      beforeEach(() => {
        tk.freeze(time);

        // calculate tomorrow's date
        const today = new Date();
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        return Promise.all([
          Checkout.create({
            userId: testUserHelper.user.id,
            lockId: uuid.v4(),
            status: 'out',
            checkOutDate: today,
            checkInDate: null,
            dueDate: tomorrow,
          }),
          Checkout.create({
            userId: uuid.v4(),
            lockId: uuid.v4(),
            status: 'out',
            checkOutDate: today,
            dueDate: tomorrow,
          }),
        ]).then(([checkout1]) => {
          checkout = checkout1.toJSON();
        });
      });

      it('should return the users checkouts', () => {
        return supertest(app)
          .get('/checkouts')
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, [
              {
                id: checkout.id,
                userId: testUserHelper.user.id,
                lockId: checkout.lockId,
                status: 'out',
                checkOutDate: '2017-03-24T21:37:00.000Z',
                checkInDate: null,
                dueDate: '2017-03-25T21:37:00.000Z',
                createdAt: '2017-03-24T21:37:00.000Z',
                updatedAt: '2017-03-24T21:37:00.000Z',
              },
            ]);
          });
      });
    });

    describe('and status param is out with no records found', () => {

      it('should return empty array', () => {
        return supertest(app)
          .get('/checkouts')
          .query({ status: 'out' })
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, []);
          });
      });
    });

    describe('and status param is out', () => {

      let checkout;
      const time = new Date(1490391420000); // 2017-03-24T21:37:00.000Z

      beforeEach(() => {
        tk.freeze(time);

        // calculate tomorrow's date
        const today = new Date();
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        return Promise.all([
          Checkout.create({
            userId: testUserHelper.user.id,
            lockId: uuid.v4(),
            status: 'out',
            checkOutDate: today,
            checkInDate: null,
            dueDate: tomorrow,
          }),
          Checkout.create({
            userId: testUserHelper.user.id,
            lockId: uuid.v4(),
            status: 'return',
            checkOutDate: today,
            dueDate: tomorrow,
          }),
        ]).then(([checkout1]) => {
          checkout = checkout1.toJSON();
        });
      });

      it('should return only the checkouts that are out', () => {
        return supertest(app)
          .get('/checkouts')
          .query({ status: 'out' })
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, [
              {
                id: checkout.id,
                userId: testUserHelper.user.id,
                lockId: checkout.lockId,
                status: 'out',
                checkOutDate: '2017-03-24T21:37:00.000Z',
                checkInDate: null,
                dueDate: '2017-03-25T21:37:00.000Z',
                createdAt: '2017-03-24T21:37:00.000Z',
                updatedAt: '2017-03-24T21:37:00.000Z',
              },
            ]);
          });
      });
    });
  });

  describe('when POST /checkouts is called', () => {
    describe('and no token is sent', () => {

      it('should return 401', () => {
        return supertest(app)
            .post('/checkouts')
            .expect(401);
      });
    });

    describe('and no params are sent', () => {

      it('should return 401', () => {
        return supertest(app)
          .post('/checkouts')
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(400)
          .then((res) => {
            assert.equal(res.body.error, 'Bad Request');
          });
      });
    });

    describe('and lockId is invalid', () => {

      it('should return 400', () => {
        return supertest(app)
          .post('/checkouts')
          .send({
            userId: testUserHelper.user.id,
            lockId: 'invalid-lock-id',
          })
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(400);
      });
    });

    describe('and userId is specified', () => {

      let lock1;
      const time = new Date(1490391420000); // 2017-03-24T21:37:00.000Z

      beforeEach(() => {
        tk.freeze(time);
        return Lock.create({ name: 'testlock1' }).then((created) => {
          return Lock.where('id', created.id).fetch();
        }).then((lock) => {
          lock1 = lock.toJSON();
        });
      });

      it('should return 400', () => {
        return supertest(app)
          .post('/checkouts')
          .send({
            userId: testUserHelper.user.id,
            lockId: lock1.id,
          })
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(400);
      });
    });

    describe('and all required params are sent', () => {

      let lock1;
      const time = new Date(1490391420000); // 2017-03-24T21:37:00.000Z

      beforeEach(() => {
        tk.freeze(time);
        return Lock.create({ name: 'testlock1' }).then((created) => {
          return Lock.where('id', created.id).fetch();
        }).then((lock) => {
          lock1 = lock.toJSON();
        });
      });

      it('should return 201', () => {
        return supertest(app)
          .post('/checkouts')
          .send({
            lockId: lock1.id,
          })
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(201)
          .then((res) => {
            assert.deepEqual(res.body, {
              id: res.body.id,
              userId: testUserHelper.user.id,
              lockId: lock1.id,
              status: 'out',
              checkOutDate: '2017-03-24T21:37:00.000Z',
              checkInDate: null,
              dueDate: '2017-03-25T21:37:00.000Z',
              createdAt: '2017-03-24T21:37:00.000Z',
              updatedAt: '2017-03-24T21:37:00.000Z',
            });
          });
      });
    });
  });

  describe('when POST /checkouts/:id/checkin is called', () => {
    describe('and no token is sent', () => {

      it('should return 401', () => {
        return supertest(app)
          .post('/checkouts/random-uuid/checkin')
          .expect(401);
      });
    });

    describe('and id is invalid', () => {

      it('should return 404', () => {
        return supertest(app)
          .post('/checkouts/random-uuid/checkin')
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(404);
      });
    });

    describe('and checkout belongs to another user', () => {

      let checkout;
      const time = new Date(1490391420000); // 2017-03-24T21:37:00.000Z

      beforeEach(() => {
        tk.freeze(time);

        // calculate tomorrow's date
        const today = new Date();
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        return Checkout.create({
          userId: uuid.v4(),
          lockId: uuid.v4(),
          status: 'out',
          checkOutDate: today,
          dueDate: tomorrow,
        }).then((created) => {
          checkout = created.toJSON();
        });
      });


      it('should return 403', () => {
        return supertest(app)
          .post(`/checkouts/${checkout.id}/checkin`)
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(403);
      });
    });

    describe('and checkout status is returned', () => {

      let checkout;
      const time = new Date(1490391420000); // 2017-03-24T21:37:00.000Z

      beforeEach(() => {
        tk.freeze(time);

        // calculate tomorrow's date
        const today = new Date();
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        return Checkout.create({
          userId: testUserHelper.user.id,
          lockId: uuid.v4(),
          status: 'returned',
          checkOutDate: today,
          checkInDate: tomorrow,
          dueDate: tomorrow,
        }).then((created) => {
          checkout = created.toJSON();
        });
      });


      it('should return 400', () => {
        return supertest(app)
          .post(`/checkouts/${checkout.id}/checkin`)
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(400)
          .then((res) => {
            assert.deepEqual(res.body, {
              message: "A status of 'out' is required for checkin",
            });
          });
      });
    });
  });

  describe('and is successful', () => {

    let checkout;
    const checkOutTime = new Date(1490391420000); // 2017-03-24T21:37:00.000Z
    const checkInTime = new Date(1490402220000); // 2017-03-25T00:37:00.000Z
    beforeEach(() => {
      tk.freeze(checkOutTime);

      // calculate tomorrow's date
      const today = new Date();
      const tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);

      return Checkout.create({
        userId: testUserHelper.user.id,
        lockId: uuid.v4(),
        status: 'out',
        checkOutDate: today,
        dueDate: tomorrow,
      }).then((created) => {
        checkout = created.toJSON();

        // free to future time
        tk.freeze(checkInTime);
      });
    });


    it('should return 200 and record', () => {
      return supertest(app)
        .post(`/checkouts/${checkout.id}/checkin`)
        .set('Authorization', testUserHelper.getBearerToken())
        .expect(200)
        .then((res) => {
          assert.deepEqual(res.body, {
            id: checkout.id,
            userId: testUserHelper.user.id,
            lockId: checkout.lockId,
            status: 'returned',
            checkOutDate: '2017-03-24T21:37:00.000Z',
            checkInDate: '2017-03-25T00:37:00.000Z',
            dueDate: '2017-03-25T21:37:00.000Z',
            createdAt: '2017-03-24T21:37:00.000Z',
            updatedAt: '2017-03-25T00:37:00.000Z',
          });
        });
    });
  });
});
