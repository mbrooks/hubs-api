const assert = require('power-assert');
const supertest = require('supertest-as-promised');
const tk = require('timekeeper');
const uuid = require('uuid');
const app = require('../../lib/server');
const db = require('../../lib/db');
const testUserHelper = require('../helpers/testUser');

const Agreement = db.model('Agreement');
const User = db.model('User');

describe('the Agreement controller', () => {

  beforeEach(() => {
    return Promise.all([
      User.destroyAll(),
    ]).then(() => {
      return testUserHelper.setup();
    });
  });

  afterEach(() => {
    tk.reset();
  });

  describe('when GET /agreements is called', () => {
    describe('and no token is sent', () => {

      it('should return 401', () => {
        return supertest(app)
          .get('/agreements')
          .expect(401);
      });
    });

    describe('and no signed agreements', () => {

      it('should return an empty array', () => {
        return supertest(app)
          .get('/agreements')
          .set('Authorization', testUserHelper.getBearerToken())
          .expect('Content-Type', /json/)
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, []);
          });
      });
    });

    describe('and agreements are signed', () => {

      let agreement1;
      beforeEach(() => {
        return Promise.all([
          Agreement.create({
            userId: testUserHelper.user.id,
            ofAge: true,
            readTermsAndConditions: 'MB',
            agreeToWaveRights: 'MB',
            version: 'MAR-01-2017',
          }),
          Agreement.create({
            userId: uuid.v4(),
            ofAge: true,
            readTermsAndConditions: 'MB',
            agreeToWaveRights: 'MB',
            version: 'MAR-01-2017',
          }),
        ]).then(([created]) => {
          return Agreement.where('id', created.id).fetch();
        }).then((agreement) => {
          agreement1 = agreement.toJSON();
        });
      });

      it('should only return agreements belonging to user', () => {
        return supertest(app)
          .get('/agreements')
          .set('Authorization', testUserHelper.getBearerToken())
          .expect('Content-Type', /json/)
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, [
              {
                id: agreement1.id,
                userId: testUserHelper.user.id,
                ofAge: true,
                readTermsAndConditions: 'MB',
                agreeToWaveRights: 'MB',
                version: 'MAR-01-2017',
                createdAt: agreement1.createdAt.toISOString(),
                updatedAt: agreement1.updatedAt.toISOString(),
              },
            ]);
          });
      });
    });

    describe('and user has multiple agreements and no filters specified', () => {

      let agreement1;
      let agreement2;
      beforeEach(() => {
        const time1 = new Date(1490391420000); // 2017-03-24T21:37:00.000Z
        tk.freeze(time1);

        return Agreement.create({
          userId: testUserHelper.user.id,
          ofAge: true,
          readTermsAndConditions: 'MB',
          agreeToWaveRights: 'MB',
          version: 'JAN-01-2017',
        }).then((created) => {
          agreement1 = created.toJSON();

          const time2 = new Date(1490403120000); // 2017-03-25T00:52:00.000Z
          tk.freeze(time2);

          return Agreement.create({
            userId: testUserHelper.user.id,
            ofAge: true,
            readTermsAndConditions: 'MB',
            agreeToWaveRights: 'MB',
            version: 'MAR-01-2017',
          });
        }).then((created) => {
          agreement2 = created.toJSON();
        });
      });

      it('should return mulitple agreements', () => {
        return supertest(app)
          .get('/agreements')
          .set('Authorization', testUserHelper.getBearerToken())
          .expect('Content-Type', /json/)
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, [
              {
                id: agreement1.id,
                userId: testUserHelper.user.id,
                ofAge: true,
                readTermsAndConditions: 'MB',
                agreeToWaveRights: 'MB',
                version: 'JAN-01-2017',
                createdAt: agreement1.createdAt.toISOString(),
                updatedAt: agreement1.updatedAt.toISOString(),
              },
              {
                id: agreement2.id,
                userId: testUserHelper.user.id,
                ofAge: true,
                readTermsAndConditions: 'MB',
                agreeToWaveRights: 'MB',
                version: 'MAR-01-2017',
                createdAt: agreement2.createdAt.toISOString(),
                updatedAt: agreement2.updatedAt.toISOString(),
              },
            ]);
          });
      });
    });

    describe('and filter by version', () => {

      let agreement1;
      beforeEach(() => {
        const time1 = new Date(1490391420000); // 2017-03-24T21:37:00.000Z
        tk.freeze(time1);

        return Agreement.create({
          userId: testUserHelper.user.id,
          ofAge: true,
          readTermsAndConditions: 'MB',
          agreeToWaveRights: 'MB',
          version: 'JAN-01-2017',
        }).then((created) => {
          agreement1 = created.toJSON();

          const time2 = new Date(1490403120000); // 2017-03-25T00:52:00.000Z
          tk.freeze(time2);

          return Agreement.create({
            userId: testUserHelper.user.id,
            ofAge: true,
            readTermsAndConditions: 'MB',
            agreeToWaveRights: 'MB',
            version: 'MAR-01-2017',
          });
        });
      });

      it('should return mulitple agreements', () => {
        return supertest(app)
          .get('/agreements')
          .query({ version: 'JAN-01-2017' })
          .set('Authorization', testUserHelper.getBearerToken())
          .expect('Content-Type', /json/)
          .expect(200)
          .then((res) => {
            assert.deepEqual(res.body, [
              {
                id: agreement1.id,
                userId: testUserHelper.user.id,
                ofAge: true,
                readTermsAndConditions: 'MB',
                agreeToWaveRights: 'MB',
                version: 'JAN-01-2017',
                createdAt: agreement1.createdAt.toISOString(),
                updatedAt: agreement1.updatedAt.toISOString(),
              },
            ]);
          });
      });
    });
  });

  describe('when POST /agreements is called', () => {
    describe('and no token is sent', () => {

      it('should return 401', () => {
        return supertest(app)
          .post('/agreements')
          .expect(401);
      });
    });

    describe('and no required params sent', () => {

      it('should return 400', () => {
        return supertest(app)
          .post('/agreements')
          .set('Authorization', testUserHelper.getBearerToken())
          .expect(400);
      });
    });

    describe('and all required params sent', () => {

      it('should return 201', () => {
        return supertest(app)
          .post('/agreements')
          .send({
            ofAge: true,
            readTermsAndConditions: 'MB',
            agreeToWaveRights: 'MB',
            version: 'JAN-01-2017',
          })
          .set('Authorization', testUserHelper.getBearerToken())
          .expect('Content-Type', /json/)
          .expect(201)
          .then((res) => {
            assert.deepEqual(res.body, {
              id: res.body.id,
              userId: testUserHelper.user.id,
              ofAge: true,
              readTermsAndConditions: 'MB',
              agreeToWaveRights: 'MB',
              version: 'JAN-01-2017',
              createdAt: res.body.createdAt,
              updatedAt: res.body.updatedAt,
            });
          });
      });
    });
  });
});
