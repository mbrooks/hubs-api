const assert = require('power-assert');
const supertest = require('supertest-as-promised');
const app = require('../../lib/server');
const db = require('../../lib/db');
const testUserHelper = require('../helpers/testUser');

const User = db.model('User');
const Lock = db.model('Lock');

describe('the Lock controller', () => {

  beforeEach(() => {
    return Promise.all([
      User.destroyAll(),
      Lock.destroyAll(),
    ]).then(() => {
      return testUserHelper.setup();
    });
  });

  describe('when GET /locks is called', () => {
    describe('and no token is sent', () => {

      it('should return 401', () => {
        return supertest(app)
            .get('/locks')
            .expect(401);
      });
    });

    describe('and there is one lock', () => {

      let lock1;

      beforeEach(() => {
        return Lock.create({ name: 'testlock1' }).then((created) => {
          return Lock.where('id', created.id).fetch();
        }).then((lock) => {
          lock1 = lock.toJSON();
        });
      });

      it('should return list of locks', () => {
        return supertest(app)
            .get('/locks')
            .expect(200)
            .set('Authorization', testUserHelper.getBearerToken())
            .expect(200)
            .then((res) => {
              assert.deepEqual(res.body, [
                {
                  id: lock1.id,
                  name: 'testlock1',
                  createdAt: lock1.createdAt.toISOString(),
                  updatedAt: lock1.updatedAt.toISOString(),
                },
              ]);
            });
      });
    });

    describe('and there are multiple locks', () => {

      let lock1;
      let lock2;

      beforeEach(() => {
        return Promise.all([
          Lock.create({ name: 'testlock1' }),
          Lock.create({ name: 'testlock2' }),
        ]).then(([created1, created2]) => {
          return Promise.all([
            Lock.where('id', created1.id).fetch(),
            Lock.where('id', created2.id).fetch(),
          ]);
        }).then(([newLock1, newLock2]) => {
          lock1 = newLock1.toJSON();
          lock2 = newLock2.toJSON();
        });
      });

      it('should return list of locks', () => {
        return supertest(app)
            .get('/locks')
            .expect(200)
            .set('Authorization', testUserHelper.getBearerToken())
            .expect(200)
            .then((res) => {
              const firstLock = res.body.find(lock => lock.name === 'testlock1');
              assert.deepEqual(firstLock, {
                id: lock1.id,
                name: 'testlock1',
                createdAt: lock1.createdAt.toISOString(),
                updatedAt: lock1.updatedAt.toISOString(),
              });

              const secondLock = res.body.find(lock => lock.name === 'testlock2');
              assert.deepEqual(secondLock, {
                id: lock2.id,
                name: 'testlock2',
                createdAt: lock2.createdAt.toISOString(),
                updatedAt: lock2.updatedAt.toISOString(),
              });
            });
      });
    });
  });
});
