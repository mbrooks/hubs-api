const bcrypt = require('bcryptjs');
const db = require('../../lib/db');
const error = require('../../lib/error');

const User = db.model('User');

exports.login = (req, res, next) => {
  const { email, password } = req.body;

  // first lookup user by email
  User.where({ email }).fetch().then((user) => {
    // if email not found, throw error
    if (!user) {
      res.status(401).json('Unauthorized');
      return;
    }

    // check password
    bcrypt.compare(password, user.get('password'), (err, result) => {
      if (err) {
        res.status(401).json('Unauthorized');
        return;
      }

      if (!result) {
        res.status(401).json('Unauthorized');
        return;
      }

      const response = user.apiResponse();
      response.token = user.getToken(); // get or create token
      res.json(response);
      return;
    });
  }).catch(next);
};

exports.ping = (req, res, next) => {
  res.json(req.user);
};

exports.register = (req, res, next) => {
  const { firstName, lastName, email, password,
    street1, street2, city, state, postalCode } = req.body;

  User.create({
    firstName,
    lastName,
    email,
    password,
    street1,
    street2,
    city,
    state,
    postalCode
  }).then((result) => {
    User.where('id', result.get('id')).fetch().then((user) => {
      user.token = user.getToken(); // get or create token
      res.status(201).json(user.apiResponse());
    });
  }).catch((err) => {
    if (err.code === "ER_DUP_ENTRY") {
      next(new error.BadRequestError('A user has already been registered with the email address specified.'));
      return;
    }
    next(err);
  });
};

exports.update = (req, res, next) => {
  const { id } = req.params;
  const { firstName, lastName, email, street1, street2, city, state, postalCode } = req.body;

  if (req.user.id !== id) {
    next(new error.ForbiddenError());
    return;
  }

  User.where('id', id).fetch().then((user) => {
    user.update({
      firstName,
      lastName,
      email,
      street1,
      street2,
      city,
      state,
      postalCode
    });
    res.status(200).json(user.apiResponse());
  }).then((result) => {
    User.where('id', result.get('id')).fetch().then((user) => {
      user.token = user.getToken(); // get or create token
      res.status(201).json(user.apiResponse());
    });
  }).catch((err) => {
    if (err.code === "ER_DUP_ENTRY") {
      next(new error.BadRequestError('A user has already been registered with the email address specified.'));
      return;
    }
    next(err);
  });
};
