const db = require('../../lib/db');

const Lock = db.model('Lock');

exports.list = (req, res, next) => {
  Lock.fetchAll().then((locks) => {
      res.json(locks.toJSON());
  }).catch(next);
};

exports.find = (req, res, next) => {
  const { id } = req.params;
  Lock.where({ id }).fetch().then((lock) => {
      // if lock not found, throw error
      if (!lock) {
        res.status(404).json('Not found');
        return;
      }

      res.json(lock.toJSON());
  }).catch(next);
};
