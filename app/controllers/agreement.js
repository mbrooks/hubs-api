const db = require('../../lib/db');

const Agreement = db.model('Agreement');

exports.add = (req, res, next) => {
  const { ofAge, readTermsAndConditions, agreeToWaveRights, version } = req.body;
  const userId = req.user.id;

  Agreement.create({
    userId,
    ofAge,
    readTermsAndConditions,
    agreeToWaveRights,
    version,
  }).then((created) => {
    return Agreement.where('id', created.id).fetch();
  }).then((agreement) => {
    res.status(201).json(agreement.apiResponse());
  }).catch(next);
};

exports.list = (req, res, next) => {
  const { version } = req.query;
  const query = {
    'userId': req.user.id,
  };

  if (version) {
    query.version = version;
  }

  Agreement.where(query).orderBy('createdAt').fetchAll().then((agreements) => {
    res.json(agreements.map(agreement => agreement.apiResponse()));
  });
};
