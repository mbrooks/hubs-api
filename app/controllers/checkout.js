const error = require('../../lib/error');
const db = require('../../lib/db');

const Checkout = db.model('Checkout');
const Lock = db.model('Lock');
const User = db.model('User');

exports.list = (req, res, next) => {
  const { status } = req.query;
  const query = {
    'userId': req.user.id,
  };

  if (status) {
    query.status = status;
  }

  Checkout.where(query).orderBy('createdAt').fetchAll().then((checkouts) => {
    res.status(200).json(checkouts.toJSON());
  }).catch(next);
};

exports.add = (req, res, next) => {
  const { lockId } = req.body;

  // first verify that lock exists
  Lock.where('id', lockId).fetch().then((lock) => {
    // calculate tomorrow's date
    const checkOutDate = new Date();
    const dueDate = new Date();
    dueDate.setDate(dueDate.getDate() + 1);

    // create the checkout record
    return Checkout.create({
      userId: req.user.id,
      lockId,
      status: 'out',
      checkOutDate,
      dueDate,
    });
  }).then((created) => {
    // looking newly created checkout
    return Checkout.where('id', created.id).fetch();
  }).then((checkout) => {
    res.status(201).json(checkout);
  }).catch(next);
};

exports.checkin = (req, res, next) => {
  const { id } = req.params;
  Checkout.where('id', id).fetch().then((checkout) => {
    if (!checkout) {
      throw new error.NotFoundError;
    }

    // make sure user has permissions to edit record
    if (checkout.get('userId') !== req.user.id) {
      throw new error.ForbiddenError;
    }

    // if status is not 'out' throw a BadRequestError
    if (checkout.get('status') !== 'out') {
      throw new error.BadRequestError("A status of 'out' is required for checkin");
    }

    const checkInDate = new Date;
    checkout.set('status', 'returned');
    checkout.set('checkInDate', checkInDate);

    return checkout.save();
  }).then((updated) => {
    res.json(updated);
  }).catch(next);
};
