const Base = require('./Base');
const joi = require('joi');
const uuid = require('uuid');
const bcrypt = require('bcryptjs');
const { omit } = require('lodash');

const CheckoutModel = Base.extend({
  tableName: 'checkouts',
  uuid: true,
  hasTimestamps: ['createdAt', 'updatedAt'],
  schema: joi.object().keys({
    id: joi.string().uuid().required(),
    userId: joi.string().uuid().required(),
    lockId: joi.string().uuid().required(),
    checkOutDate: joi.date().required(),
    checkInDate: joi.date().allow(null),
    dueDate: joi.date().required(),
    status: joi.string().allow('out', 'returned').required(),
    createdAt: joi.date().required(),
    updatedAt: joi.date().required(),
  }),

  initialize() {
    Base.prototype.initialize.call(this);
  },
});

module.exports = CheckoutModel;
