const Base = require('./Base');
const joi = require('joi');
const uuid = require('uuid');
const bcrypt = require('bcryptjs');
const { omit } = require('lodash');

const LockModel = Base.extend({
  tableName: 'locks',
  uuid: true,
  hasTimestamps: ['createdAt', 'updatedAt'],
  schema: joi.object().keys({
    id: joi.string().uuid().required(),
    name: joi.string().min(3).required(),
    createdAt: joi.date().required(),
    updatedAt: joi.date().required(),
  }),

  initialize() {
    Base.prototype.initialize.call(this);
  },
});

module.exports = LockModel;
