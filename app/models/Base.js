const joi = require('joi');
const uuid = require('uuid');
const bookshelf = require('../../lib/bookshelf');

const BaseModel = bookshelf.Model.extend({
  schema: joi.object().keys({}),

  initialize() {
    this.on('creating', this.addId);
    this.on('saving', this.validateSave);
  },

  addId(model) {
    const id = model.get(model.idAttribute);
    if (this.uuid && !id) {
      model.set({ id: uuid.v4() });
    }
  },

  validateSave() {
    return joi.validate(this.attributes, this.schema, (err) => {
      if (err) {
        throw new Error(err);
      }
    });
  },

  update(params) {
    this.set(params);
    return this.save();
  },
}, {
  create(params) {
    return this.forge(params).save();
  },

  destroyAll() {
    return this.query().delete();
  },
});

module.exports = BaseModel;
