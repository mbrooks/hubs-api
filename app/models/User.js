const Base = require('./Base');
const joi = require('joi');
const uuid = require('uuid');
const bcrypt = require('bcryptjs');
const { omit } = require('lodash');

const UserModel = Base.extend({
  tableName: 'users',
  uuid: true,
  hasTimestamps: ['createdAt', 'updatedAt'],
  schema: joi.object().keys({
    id: joi.string().uuid().required(),
    token: joi.string(),
    firstName: joi.string().min(2).required(),
    lastName: joi.string().min(2).required(),
    email: joi.string().email().required(),
    password: joi.string().min(6).required(),
    street1: joi.string().min(3).required(),
    street2: joi.string().min(3).allow(null, ''),
    city: joi.string().min(3).required(),
    state: joi.string().min(2).max(2).required(),
    postalCode: joi.string().min(5).required(),
    createdAt: joi.date().required(),
    updatedAt: joi.date().required(),
  }),

  initialize() {
    Base.prototype.initialize.call(this);
    this.on('saving', this.hashPassword);
  },

  apiResponse() {
    const pojo = this.toJSON();
    const response = omit(pojo, ['password']);
    return response;
  },

  getToken() {
    let token = this.get('token');
    if (!token) {
      token = uuid.v4();
      this.set('token', token);
      this.save();
    }

    return token;
  },

  hashPassword() {
    if (this.hasChanged('password') && this.get('password')) {
      const hash = bcrypt.hashSync(this.get('password'), 8);
      this.set('password', hash);
    }
  },

  roles() {
    return this.hasMany('UserRole', 'userId');
  },
});

module.exports = UserModel;
