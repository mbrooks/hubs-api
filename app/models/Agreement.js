const Base = require('./Base');
const joi = require('joi');
const uuid = require('uuid');
const bcrypt = require('bcryptjs');

function booleanValue(value) {
  if (value === 1) {
    return true;
  }

  return false;
}

const AgreementModel = Base.extend({
  tableName: 'agreements',
  uuid: true,
  hasTimestamps: ['createdAt', 'updatedAt'],
  schema: joi.object().keys({
    id: joi.string().uuid().required(),
    userId: joi.string().uuid().required(),
    ofAge: joi.boolean().required(),
    readTermsAndConditions: joi.string().max(2).required(),
    agreeToWaveRights: joi.string().max(2).required(),
    version: joi.string().required(),
    createdAt: joi.date().required(),
    updatedAt: joi.date().required(),
  }),

  initialize() {
    Base.prototype.initialize.call(this);
  },

  apiResponse() {
    const response = this.toJSON();
    response.ofAge = booleanValue(response.ofAge);
    return response;
  },
});

module.exports = AgreementModel;
