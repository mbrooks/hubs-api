const joi = require('joi');
const celebrate = require('celebrate');
const log = require('../lib/log');
const agreement = require('./controllers/agreement');
const checkout = require('./controllers/checkout');
const lock = require('./controllers/lock');
const user = require('./controllers/user');
const passport = require('../lib/passport');
const db = require('../lib/db');

// include tokenRequired to enable passport authentication
const tokenRequired = passport.authenticate('bearer', { session: false });

module.exports = (app) => {
  // agreement routes
  app.get('/agreements', celebrate({
    query: {
      version: joi.string(),
    },
  }), tokenRequired, agreement.list);

  app.post('/agreements', tokenRequired, celebrate({
    body: {
      ofAge: joi.boolean().required(),
      readTermsAndConditions: joi.string().max(2).required(),
      agreeToWaveRights: joi.string().max(2).required(),
      version: joi.string().required(),
    },
  }), agreement.add);

  // checkout routes
  app.get('/checkouts', celebrate({
    query: {
      status: joi.string(),
    },
  }), tokenRequired, checkout.list);

  app.post('/checkouts', tokenRequired, celebrate({
    body: joi.object().keys({
      lockId: joi.string().uuid().required(),
    }),
  }), checkout.add);

  app.post('/checkouts/:id/checkin', tokenRequired, checkout.checkin);

  // lock routes
  app.get('/locks', tokenRequired, lock.list);
  app.get('/locks/:id', tokenRequired, lock.find);

  // user routes
  app.post('/user/login', celebrate({
    body: joi.object().keys({
      email: joi.string().required(),
      password: joi.string().required(),
    }),
  }), user.login);

  // user routes
  app.get('/user/ping', tokenRequired, user.ping);

  app.post('/user/register', celebrate({
    body: joi.object().keys({
      firstName: joi.string().required(),
      lastName: joi.string().required(),
      email: joi.string().required(),
      password: joi.string().required(),
      street1: joi.string().required(),
      street2: joi.string().allow(null),
      city: joi.string().required(),
      state: joi.string().required(),
      postalCode: joi.string().required(),
    }),
  }), user.register);

  app.post('/user/:id', tokenRequired, celebrate({
    body: joi.object().keys({
      firstName: joi.string().required(),
      lastName: joi.string().required(),
      email: joi.string().required(),
      street1: joi.string().required(),
      street2: joi.string().allow(null),
      city: joi.string().required(),
      state: joi.string().required(),
      postalCode: joi.string().required(),
    }),
  }), user.update);

  // handle errors
  app.use(celebrate.errors());
  app.use((err, req, res, next) => {
    log.debug(err);

    // if the error has a status, return the error with that status
    if (err.status) {
      return res.status(err.status).json({ message: err.message });
    }

    // handle joi validation errors
    const validationError = /^ValidationError:.*/;
    if (validationError.test(err.message)) {
      return res.status(400).json({ message: err.message });
    }

    if (err instanceof db.NotFoundError) {
      return res.status(404).json({ message: '404 Not found' });
    }

    // error page
    return res.status(500).json({ message: 'Internal Server Error' });
  });

  // assume 404 since no middleware responded
  app.use((req, res) => {
    res.status(404).json({ message: '404 Not found' });
  });
};
