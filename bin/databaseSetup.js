#!/usr/bin/env node

const config = require('config');
const knexFactory = require('knex');
const log = require('../lib/log');

const dbName = config.knex.connection.database;
delete config.knex.connection.database;
let knex = knexFactory(config.knex);
let dropDb = false;
let actions;

if (process.argv.length > 1) {
  if (process.argv.includes('--drop-db')) {
    dropDb = true;
  }
}

// drop database if arguement found
if (dropDb) {
  actions = knex.raw(`DROP DATABASE IF EXISTS ${dbName}`).then(() => {
    log.info(`Dropped Database: ${dbName}`);
  });
} else {
  actions = new Promise();
}

actions.then(() => knex.raw(`CREATE DATABASE ${dbName}`)).then(() => {
  log.info(`Created Database: ${dbName}`);

  // reinit the knex db connection
  knex.destroy();
  config.knex.connection.database = dbName;
  knex = knexFactory(config.knex);

  return knex.migrate.latest();
}).then(() => {
  log.info(`Migrated Database: ${dbName}`);
  process.exit(0);
}).catch((err) => {
  log.error(err.message);
  process.exit(1);
});
