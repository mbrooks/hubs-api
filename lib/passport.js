const passport = require('passport');
const BearerStrategy = require('passport-http-bearer');
const db = require('./db');

const User = db.model('User');

passport.use(new BearerStrategy((token, done) => {
  User.where({ token }).fetch().then((user) => {
    if (user) {
      return done(null, user.apiResponse(), { scope: 'all' });
    }
    return done(null, false);
  });
}));

module.exports = passport;
