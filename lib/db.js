const bookshelf = require('./bookshelf');
const Agreement = require('../app/models/Agreement');
const Checkout = require('../app/models/Checkout');
const User = require('../app/models/User');
const Lock = require('../app/models/Lock');

bookshelf.model('Agreement', Agreement);
bookshelf.model('Checkout', Checkout);
bookshelf.model('Lock', Lock);
bookshelf.model('User', User);

module.exports = bookshelf;
