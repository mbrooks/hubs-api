const config = require('config');
const knexFactory = require('knex');
const bookshelfFactory = require('bookshelf');

const knex = knexFactory(config.knex);
const bookshelf = bookshelfFactory(knex);

bookshelf.plugin('registry');

module.exports = bookshelf;
