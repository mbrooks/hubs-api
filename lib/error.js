module.exports = {
  BadRequestError: class BadRequestError extends Error {
    constructor(message) {
      super(message);
      this.status = 400;
    }
  },

  ForbiddenError: class ForbiddenError extends Error {
    constructor(message = 'Forbidden') {
      super(message);
      this.status = 403;
    }
  },

  NotFoundError: class NotFoundError extends Error {
    constructor(message) {
      super(message);
      this.status = 404;
    }
  },
};
