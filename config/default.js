module.exports = {
  server: {
    port: 2610,
  },
  knex: {
    client: 'mysql',
    connection: {
      host: '127.0.0.1',
      user: 'hubs',
      password: 'notforprofit$',
      database: 'hubs',
      charset: 'utf8mb4',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
};
